package model;

public class Field {

    private int x;
    private int y;
    private int h;
    private int w;

    public Field() {
        this.x = (int)(Math.random()*(((Model.getWIDTH()-75)-50)))+75;
        this.y = (int)(Math.random()*((-30-(-Model.getHEIGHT()+30))))+30;
        this.h = 30;
        this.w = 30;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getH() {
        return h;
    }

    public int getW() {
        return w;
    }

}
