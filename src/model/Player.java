package model;

public class Player {

    private int x;
    private int y;
    private int h;
    private int w;
    private boolean notHit;
    private int score;
    private int level;
    private int live;

    public Player(int x, int y) {
        this.x = x;
        this.y = y;
        this.h = 20;
        this.w = 20;
        this.notHit = true;
        this.live = 3;
        this.score = 0;
        this.level = 1;
    }

    public void move(int dx, int dy) {
        this.x += dx;
        this.y += dy;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getH() {
        return h;
    }

    public int getW() {
        return w;
    }

    public boolean getNotHit() {
        return notHit;
    }

    public void setNotHit(boolean notHit) {
        this.notHit = notHit;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public int getLive() {
        return live;
    }

    public void setLive(int live) {
        this.live = live;
    }
}

