package model;

import java.util.ArrayList;
import java.util.List;

public class Model {

    private Player player;
    private List<Enemy> enemyList = new ArrayList<>();
    private List<Item> itemList = new ArrayList<>();
    private List<Field> fieldList = new ArrayList<>();

    private static final double WIDTH = 900;
    private static final double HEIGHT = 500;

    private int anzEnemys = 15;
    private int anzItemDiamond = 7;
    private int anzItemExtraLive = 2;
    private int anzFieldElements = 15;

    public Model() {

        initialNewGame();
    }

    public void update() {
        playerUpdate();
        enemyUpdate();
        hitField();
        updatePlayerCollision();
        updateItemCollection();
    }

    private void playerUpdate() {
        player.move(0, 0);
    }

    private void enemyUpdate() {
        for (model.Enemy enemy : enemyList) {
            enemy.move();

            // Hit-Border
            if (enemy.getX() >= (getWIDTH() - enemy.getW()) || enemy.getX() <= 0) {
                enemy.setDx(-enemy.getDx());
            }

            if (enemy.getY() <= 0 || enemy.getY() >= (getHEIGHT() - enemy.getH())) {
                enemy.setDy(-enemy.getDy());
            }
        }

        updateLevel(player, enemyList);
    }

    // Hit-Field
    private void hitField() {
        for (model.Enemy enemy : enemyList) {
            for (model.Field field : fieldList) {

                if (collisionField(enemy, field)) {
                    enemy.setDx(-enemy.getDx());
                    enemy.setDy(-enemy.getDy());

                }
            }
        }
    }

    // Collect Items
    private void updateItemCollection() {
        for (model.Item item : itemList) {
            if (collisionItem(item, player)) {
                if (!item.isCollected()) {
                    if (!item.isExtraLeben()) {
                        item.setCollected(true);
                        player.setScore(player.getScore() + 1);
                    }

                    if (item.isExtraLeben()) {
                        item.setCollected(true);
                        player.setLive(player.getLive() + 1);
                    }
                }
            }
        }
    }

    // Collision Player - Enemy
    private void updatePlayerCollision() {
        for (model.Enemy enemy : enemyList) {

            if (collisionEnemy(enemy, player)) {
                enemy.setDx(-enemy.getDx());
                enemy.setDy(-enemy.getDy());

                player.setLive(player.getLive() - 1);
                if (player.getLive() == 0) {
                    player.setNotHit(false);
                }
            }
        }
    }

    // Restart Game
    public void restartGame() {

        enemyList.clear();
        itemList.clear();
        fieldList.clear();
        initialNewGame();

    }

    private void initialNewGame() {
        this.player = new Player(50, 250);

        for (int i = 0; i < anzEnemys / 2; i++) {
            enemyList.add(new Enemy(1, 0));
            enemyList.add(new Enemy(0, 1));
        }

        for (int i = 0; i < anzItemDiamond; i++) {
            itemList.add(new Item(false));
        }

        // Extra-Leben hinzfügen
        for (int i = 0; i < anzItemExtraLive; i++) {
            itemList.add(new Item(true));
        }

        for (int i = 0; i < anzFieldElements; i++) {
            fieldList.add(new Field());
        }
    }

    private void updateLevel(Player player, List<Enemy> enemyList) {

        if (player.getLevel() == 1) {
            if (player.getScore() >= 2) {
                for (model.Enemy enemy : enemyList) {
                    enemy.setDx(enemy.getDx() * -2);
                    enemy.setDy(enemy.getDy() * -2);
                }
                player.setLevel(2);
            }
        }
        if (player.getLevel() == 2) {
            if (player.getScore() >= 4) {
                for (model.Enemy enemy : enemyList) {
                    enemy.setDx(enemy.getDx() * -1);
                    enemy.setDy(enemy.getDy() * -1);
                }
                player.setLevel(3);
            }
        }

        if (player.getLevel() == 3) {
            if (player.getScore() >= 6) {
                for (model.Enemy enemy : enemyList) {
                    enemy.setDx(enemy.getDx() * -2);
                    enemy.setDy(enemy.getDy() * -2);
                }
                player.setLevel(4);
            }
        }


    }

    private boolean collisionEnemy(Enemy enemy, Player player) {
        int yPlayer = Math.abs(player.getY());
        int yEnemy = Math.abs(enemy.getY());

        int xdiff = Math.abs(player.getX() - enemy.getX());
        int ydiff = Math.abs(yPlayer - yEnemy);

        return ((xdiff < player.getH()) && (ydiff < player.getW()));
    }

    private boolean collisionField(Enemy enemy, Field field) {
        int yField = Math.abs(field.getY());
        int yEnemy = Math.abs(enemy.getY());

        int xdiff = Math.abs(field.getX() - enemy.getX());
        int ydiff = Math.abs(yField - yEnemy);

        return ((xdiff < enemy.getH()) && (ydiff < enemy.getW()));
    }

    private boolean collisionItem(Item item, Player player) {
        int yPlayer = Math.abs(player.getY());
        int yItem = Math.abs(item.getY());

        int xdiff = Math.abs(player.getX() - item.getX());
        int ydiff = Math.abs(yPlayer - yItem);

        return ((xdiff < player.getH()) && (ydiff < player.getW()));
    }

    public Player getPlayer() {
        return player;
    }

    public List<Enemy> getEnemyList() {
        return enemyList;
    }

    public List<Item> getItemList() {
        return itemList;
    }

    public List<Field> getFieldList() {
        return fieldList;
    }

    public static double getWIDTH() {
        return WIDTH;
    }

    public static double getHEIGHT() {
        return HEIGHT;
    }

}
