package model;

import java.util.Random;

public class Enemy {

    private boolean a;
    private int x;
    private int y;
    private int h;
    private int w;
    private int dx;
    private int dy;

    public Enemy(int dx, int dy) {
        this.x = (int) (Math.random() * (((Model.getWIDTH() - 100) - 100))) + 100;
        this.y = (int) (Math.random() * ((-30 - (-Model.getHEIGHT() + 30)))) + 30;
        this.h = 25;
        this.w = 25;
        this.dx = dx;
        this.dy = dy;
    }

    public void move() {
        this.x += dx;
        this.y += dy;
    }

    public int getDx() {
        return dx;
    }

    public int getDy() {
        return dy;
    }

    public void setDx(int dx) {
        this.dx = dx;
    }

    public void setDy(int dy) {
        this.dy = dy;
    }

    /*
    public static void setDx(int dx) {
        Enemy.dx = dx;
    }

    public static void setDy(int dy) {
        Enemy.dy = dy;
    }

     */

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getH() {
        return h;
    }

    public int getW() {
        return w;
    }
}
