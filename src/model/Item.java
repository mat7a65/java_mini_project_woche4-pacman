package model;

import java.awt.*;

public class Item {

    private int x;
    private int y;
    private int h;
    private int w;
    private boolean collected;
    private boolean extraLeben;

    public Item(boolean isExtraLive) {
        this.x = (int)(Math.random()*(((Model.getWIDTH()-75)-100)))+100;
        this.y = (int)(Math.random()*((-30-(-Model.getHEIGHT()+30))))+30;
        this.h = 20;
        this.w = 20;
        this.collected = false;
        this.extraLeben = isExtraLive;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getH() {
        return h;
    }

    public int getW() {
        return w;
    }

    public boolean isCollected() {
        return collected;
    }

    public void setCollected(boolean a) {
        this.collected = a;
    }

    public boolean isExtraLeben() {
        return extraLeben;
    }

}
