import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import model.*;

import java.io.FileInputStream;
import java.io.FileNotFoundException;

import javafx.scene.image.Image;

public class Graphics {

    private GraphicsContext gc;
    private Model model;
    Image bg = new Image(new FileInputStream("src/images/bg.png"));
    Image bomb = new Image(new FileInputStream("src/images/bomb.png"));
    Image diamond = new Image(new FileInputStream("src/images/diamond.png"));
    Image heart = new Image(new FileInputStream("src/images/heart.png"));
    Image rock = new Image(new FileInputStream("src/images/rock.png"));
    Image sailor = new Image(new FileInputStream("src/images/sailor.png"));

    public Graphics(GraphicsContext gc, Model model) throws FileNotFoundException {
        this.gc = gc;
        this.model = model;
    }

    public void draw() {
        clearCanvas();
        drawBg();
        drawCounter();
        drawField();
        drawPlayer();
        drawEnemy();
        drawItem();
    }

    private void clearCanvas() {
        gc.clearRect(0, 0, Model.getWIDTH(), Model.getHEIGHT());
    }

    private void drawBg() {
        gc.drawImage(bg, 0, 0, Model.getWIDTH(), Model.getHEIGHT());
    }

    private void drawCounter() {
        // Set Counter + Level
        gc.setFill(Color.AQUAMARINE);
        gc.fillRect(Model.getWIDTH() - 350, 0, 350, 30);

        gc.setFill(Color.BLACK);
        gc.setFont(new Font("", 20));
        gc.fillText("Lives: " + model.getPlayer().getLive(), Model.getWIDTH() - 340, 22);

        gc.setFill(Color.BLACK);
        gc.setFont(new Font("", 20));
        gc.fillText("Score: " + model.getPlayer().getScore(), Model.getWIDTH() - 220, 22, 150);

        gc.setFill(Color.BLACK);
        gc.setFont(new Font("", 20));
        gc.fillText("Stage: " + model.getPlayer().getLevel(), Model.getWIDTH() - 100, 22);

    }

    private void drawPlayer() {
        {
            if (model.getPlayer().getNotHit()) {

                gc.drawImage(sailor, model.getPlayer().getX(),
                        model.getPlayer().getY(),
                        model.getPlayer().getW() + 10,
                        model.getPlayer().getH() + 10);

            } else {
                gc.setFill(Color.DARKRED);
                gc.fillRect(0, 0, Model.getWIDTH(), Model.getHEIGHT());

                gc.setFill(Color.HOTPINK);
                gc.setFont(new Font("", 60));
                gc.fillText("-- GAME OVER --\n     PRESS 'R'\n  FOR RESTART", Model.getWIDTH() / 4, 150);

            }
        }

        if (model.getPlayer().getScore() == 7) {
            gc.setFill(Color.LIGHTGREEN);
            gc.fillRect(0, 0, Model.getWIDTH(), Model.getHEIGHT());

            gc.setFill(Color.HOTPINK);
            gc.setFont(new Font("", 60));
            gc.fillText("-- YOU WON --\n     PRESS 'R'\n  FOR RESTART", Model.getWIDTH() / 4, 150);

        }
    }

    private void drawItem() {
        for (Item item : model.getItemList()) {
            if (!item.isCollected() && !item.isExtraLeben()) {
                gc.drawImage(diamond,
                        item.getX(),
                        item.getY(),
                        item.getW() + 8,
                        item.getH() + 8);
            }

            if (!item.isCollected() && item.isExtraLeben()) {

                gc.drawImage(heart, item.getX(),
                        item.getY(),
                        item.getW() + 8,
                        item.getH() + 8);

            }
        }
    }

    private void drawField() {
        for (Field field : model.getFieldList()) {

            gc.drawImage(rock, field.getX(),
                    field.getY(),
                    field.getW() + 5,
                    field.getH() + 5);
        }
    }

    private void drawEnemy() {
        for (Enemy enemy : model.getEnemyList()) {
            gc.drawImage(bomb, enemy.getX(),
                    enemy.getY(),
                    enemy.getW() + 5,
                    enemy.getH() + 5);

        }
    }
}

